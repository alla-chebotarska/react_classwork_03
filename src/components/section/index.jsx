import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import { data } from '../reques';

export default class Section extends Component {

    state = { allCurrency: null }
    componentDidMount() {
        data(this.props.url)
            .then((data1) => {
                this.setState(({ allCurrency }) => {
                    return {
                        allCurrency: data1
                    } // повертає новий об'єкт
                })
            });
    }

    render() {
        const {allCurrency} = this.state;
        return (
            <div>
                <table>
                    <tbody>
                        <tr>
                            <th>Currency name</th>
                            <th>Price</th>
                            <th>Cod</th>
                        </tr>
                        { Array.isArray(allCurrency) ? 
                        allCurrency.map((item, index) => {
                            return (
                                <tr key={index + "t"}>
                                    <td>{item.txt}</td>
                                    <td>{item.rate ? item.rate.toFixed(2) : item.value}</td>
                                    <td>{item.cc}</td>
                                </tr>
                            )
                        }) :  <CircularProgress />}
                    </tbody>
                </table>
            </div>
        )
    }
}
