import React, { Component } from 'react';
import Button from '../button';
import Section from '../section';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import './main.css';


class Main extends Component {

    render() {
        // перший спосіб створення запиту
        /*let promise = fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
        promise.then((data) => {return data.json()})
        .then((data1)=> {console.log(data1)})
        */
        return (
            <>
            <Router>
                <header>
                    <Button value='1'></Button>
                    <Button value='2'></Button>
                    <Button value='3'></Button>
                </header>
                <Route path='/currency'>
                    <Section url='https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'></Section>
                </Route>
                <Route path='/bank-info'>
                    <Section url="https://bank.gov.ua/NBUStatService/v1/statdirectory/basindbank?date=20160101&period=m&json"></Section>
                </Route>
                
            </Router>
            </>
        )
    }

}

/* https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json */
export default Main;